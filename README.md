# Vanilla-Yo-Notification(VYN)

# This repo has been moved to github [https://github.com/cooljith91112/vanilla-yo-notification/](https://github.com/cooljith91112/vanilla-yo-notification/)
# All the development will be on github

**Vanilla-Yo-Notification(VYN)** is a light-weight **ES6** compatible **javascript** growl like notification library (JQuery Free).


Complete documentation can be found here -> [libvyn.indrajith.surge.sh](http://libvyn.indrajith.surge.sh)


### Support ME

[![](https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0)](https://ko-fi.com/R6R36EBQ)